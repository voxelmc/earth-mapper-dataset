# Earth Mapper Dataset

Welcome to the Public Domain EarthMapper Dataset.


Bathymetry data is sourced by the [GEBCO 2021 Grid](https://www.gebco.net/data_and_products/gridded_bathymetry_data/)
(EMD is not affiliated or endorsed by GEBCO, the IHO or the IOC)

<div align=right><h2>VoxelMC</h2></div>
